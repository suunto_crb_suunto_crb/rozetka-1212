package ua.com.rozetka.test;

import com.codeborne.selenide.Configuration;
import org.junit.Test;

import ua.com.rozetka.widgets.Common;
import ua.com.rozetka.widgets.Electronics;

import static com.codeborne.selenide.Selenide.open;

public class RozetkaCaseOneTest extends Common{

    {
        Configuration.timeout = 8000;
    }

    @Test
    public void sendPhoneList() {
        Electronics electronics = new Electronics();

        open("/");

        electronics.open();
        electronics.openPhones();
        electronics.openSmartphones();
        showMoreClick();
        showMoreClick();
        getProductNameList();
        writeToTxtFile();
        sendListProductsByMail("Смартфоны", "Выборка смартфонов", "кому@gmail.com", "от_кого@gmail.com", "C:/smartphones.txt");
    }
}


