package ua.com.rozetka.test;

import com.codeborne.selenide.Configuration;
import org.junit.Test;

import ua.com.rozetka.widgets.Common;
import ua.com.rozetka.widgets.HouseholdProducts;

import static com.codeborne.selenide.Selenide.open;

public class RozetkaCaseTwoTest extends Common{

    {
        Configuration.timeout = 8000;
    }

    @Test
    public void saveWashingPowderListInDb() {
        HouseholdProducts householdproducts = new HouseholdProducts();

        open("/");

        householdproducts.open();
        householdproducts.openHouseholdChemicals();
        householdproducts.openForWashing();
        householdproducts.openPowders();
        setPriceFilter(100, 300);
        showMoreClick();
        showMoreClick();
        showMoreClick();
        getProductNameList();
        getProductPriceList();
        insertDataInDB();
    }
}

