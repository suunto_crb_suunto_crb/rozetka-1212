package ua.com.rozetka.test;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.junit.Test;
import ua.com.rozetka.widgets.Common;
import ua.com.rozetka.widgets.Electronics;

import java.io.IOException;
import java.util.List;

import static com.codeborne.selenide.Selenide.open;

public class RozetkaCaseThreeTest extends Common {

    {
        Configuration.timeout = 8000;
    }

    @Test
    public void sendTopSalePhoneList() throws IOException {
        Electronics electronics = new Electronics();

        open("/");

        electronics.open();
        electronics.openPhones();
        electronics.openSmartphones();
        showMoreClick();
        showMoreClick();
        List<String> topSaleProductName = getTopSaleProductNameList();
        List<String> topSaleProductPriceList = getTopSaleProductPriceList();

        setPriceFilter(3000, 6000);
        showMoreClick();
        showMoreClick();
        showMoreClick();
        showMoreClick();
        List<String> productName = getProductNameList();
        List<String> productPrice = getProductPriceList();

        writeToExcelFile(topSaleProductName, topSaleProductPriceList, productName, productPrice);
        sendListProductsByMail("Смартфоны", "Выборка смартфонов", "recipient@gmail.com", "sender@gmail.com", "C:/top sales.xls");
    }
}


