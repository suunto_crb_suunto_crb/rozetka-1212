package ua.com.rozetka.externalClasses;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class InsertInDB {

    // JDBC URL, username and password of MySQL server
    private static final String url = "jdbc:mysql://localhost:3306/Rozetka?autoReconnect=true&useSSL=false";
    private static final String user = "root";
    private static final String password = "Supervisor";

    // JDBC variables for opening and managing connection
    private static Connection con;
    private static Statement stmt;

    public void insertQuery(List<String> name, List<String> price) {
        String query = null;

        try {
            // opening database connection to MySQL server
            con = DriverManager.getConnection(url, user, password);

            // getting Statement object to execute query
            stmt = con.createStatement();

            // executing Update query
            for (int i = 0; i < name.size(); i++) {
                String currentName = name.get(i);
                String currentPrice = price.get(i);
                query = "INSERT INTO washingpowder (ProductName, Price) VALUES ('" + currentName + "', '" + currentPrice + "');";
                stmt.executeUpdate(query);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            //close connection and stmt here
            try {
                con.close();
            } catch (SQLException se) { /*can't do anything */ }
            try {
                stmt.close();
            } catch (SQLException se) { /*can't do anything */ }
        }
    }
}
