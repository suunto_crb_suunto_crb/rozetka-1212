package ua.com.rozetka.externalClasses;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class CreateExcelFile {
    private static HSSFCellStyle createStyleForTitle(HSSFWorkbook workbook) {
        HSSFFont font = workbook.createFont();
        font.setBold(true);
        HSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font);
        return style;
    }

    public void write(List<String> topSaleProductName, List<String> topSaleProductPriceList, List<String> productName, List<String> productPrice) throws IOException {

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheetFirst = workbook.createSheet("Top sale product");
        HSSFSheet sheetSecond = workbook.createSheet("3000-6000 grn");

        int rownum = 0;
        Cell cell;
        Row rowFirst;
        Row rowSecond;
        //
        HSSFCellStyle style = createStyleForTitle(workbook);

        rowFirst = sheetFirst.createRow(rownum);
        rowSecond = sheetSecond.createRow(rownum);

        // Product name1
        cell = rowFirst.createCell(0, CellType.STRING);
        cell.setCellValue("Product name");
        cell.setCellStyle(style);
        // Product price1
        cell = rowFirst.createCell(1, CellType.STRING);
        cell.setCellValue("Product price");
        cell.setCellStyle(style);

        // Product name2
        cell = rowSecond.createCell(0, CellType.STRING);
        cell.setCellValue("Product name");
        cell.setCellStyle(style);
        // Product price2
        cell = rowSecond.createCell(1, CellType.STRING);
        cell.setCellValue("Product price");
        cell.setCellStyle(style);

//         Data1
        for (String name : topSaleProductName) {
            rowFirst = sheetFirst.createRow(rownum);

            // Product name1 (A)
            cell = rowFirst.createCell(0, CellType.STRING);
            cell.setCellValue(name);
            // Product price1 (B)
            cell = rowFirst.createCell(1, CellType.STRING);
            cell.setCellValue(topSaleProductPriceList.get(rownum));
            rownum++;
        }

        rownum = 0;
//         Data2
        for (String name : productName) {
            rowSecond = sheetSecond.createRow(rownum);

            // Product name2 (A)
            cell = rowSecond.createCell(0, CellType.STRING);
            cell.setCellValue(name);
            // Product price2 (B)
            cell = rowSecond.createCell(1, CellType.STRING);
            cell.setCellValue(productPrice.get(rownum));
            rownum++;
        }

        File file = new File("C:/top sales.xls");
        file.getParentFile().mkdirs();

        FileOutputStream outFile = new FileOutputStream(file);
        workbook.write(outFile);
        System.out.println("Created file: " + file.getAbsolutePath());
    }
}
