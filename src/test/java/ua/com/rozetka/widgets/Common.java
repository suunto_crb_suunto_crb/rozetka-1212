package ua.com.rozetka.widgets;

import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ua.com.rozetka.externalClasses.CreateExcelFile;
import ua.com.rozetka.externalClasses.InsertInDB;
import ua.com.rozetka.externalClasses.Sender;
import ua.com.rozetka.externalClasses.WriteToTxtFile;
import ua.com.rozetka.testconfigs.BaseTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Common extends BaseTest {

    private String nameLocator = ".g-i-tile-i-title.clearfix>a";
    private String priceLocator = ".g-price-uah";
    private String topSaleNameLocator = ".//i[@class='g-tag g-tag-icon-middle-popularity sprite']/../../following-sibling::div[@class='g-i-tile-i-title clearfix']/a";
    private String topSalePriceLocator = ".//i[@class='g-tag g-tag-icon-middle-popularity sprite']/../../following-sibling::div/div/div/div[@class='g-price-uah']";

    public List<String> getProductNameList() {
        List<String> productName = new ArrayList<String>();
        ElementsCollection allProductName = $$(nameLocator);
        for (WebElement w : allProductName) {
            productName.add(w.getText());
        }
        return productName;
    }

    public List<String> getProductPriceList() {
        List<String> productPrice = new ArrayList<String>();
        ElementsCollection allProductPrice = $$(priceLocator);
        for (WebElement w : allProductPrice) {
            productPrice.add(w.getText());
        }
        return productPrice;
    }

    public List<String> getTopSaleProductNameList() {
        List<String> topSaleProductName = new ArrayList<String>();
        ElementsCollection allProductName = $$(By.xpath(topSaleNameLocator));
        for (WebElement w : allProductName) {
            topSaleProductName.add(w.getText());
        }
        return topSaleProductName;
    }

    public List<String> getTopSaleProductPriceList() {
        List<String> topSaleProductPriceList = new ArrayList<String>();
        ElementsCollection allProductPrice = $$(By.xpath(topSalePriceLocator));
        for (WebElement w : allProductPrice) {
            topSaleProductPriceList.add(w.getText());
        }
        return topSaleProductPriceList;
    }

    public void writeToExcelFile(List<String> topSaleProductName, List<String> topSaleProductPriceList, List<String> productName, List<String> productPrice) throws IOException {
        CreateExcelFile excelFile = new CreateExcelFile();
        excelFile.write(topSaleProductName, topSaleProductPriceList, productName, productPrice);
    }

    public void insertDataInDB() {
        List<String> productName = getList(nameLocator);
        List<String> productPrice = getList(priceLocator);
        InsertInDB data = new InsertInDB();
        data.insertQuery(productName, productPrice);
    }

    public List<String> getList(String selector) {
        List<String> list = new ArrayList<String>();
        ElementsCollection all = $$((selector));
        for (WebElement w : all) {
            list.add(w.getText());
        }
        return list;
    }

    private List<String> productName = new ArrayList<String>();

    public void writeToTxtFile() {
        WriteToTxtFile fileName = new WriteToTxtFile();
        fileName.write(productName);
    }

    public void sendListProductsByMail(String subject, String text, String toMail, String fromMail, String attachment) {
        Sender bodyMail = new Sender();
        bodyMail.send(subject, text, toMail, fromMail, attachment);
    }

    public void showMoreClick() {
        $(".g-i-more-link-text").should(exist).click();
    }

    public void setPriceFilter(int minPrice, int maxPrice) {
        $("input#price\\[min\\]").setValue(String.valueOf(minPrice));
        $("input#price\\[max\\]").sendKeys("\b\b\b\b\b"); //для очистки поля (пользователю неудобно удалять ненужные данные, чтобы ввести свои) Я бы предложил не проставлять максимально возможное значение при клике в пустом поле
        $("input#price\\[max\\]").setValue(String.valueOf(maxPrice));
        $("button#submitprice").click();
    }
}
