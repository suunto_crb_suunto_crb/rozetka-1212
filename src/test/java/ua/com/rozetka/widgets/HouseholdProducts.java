package ua.com.rozetka.widgets;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class HouseholdProducts extends Common {

    public void open() {
        $(".logo-link").click();
        $$("nav[class~='m-main'] a").findBy(exactText("Товары для дома")).click();
    }

    public void openHouseholdChemicals() {
        $$("#menu_categories_left a").findBy(exactText("Бытовая химия")).click();
    }

    public void openForWashing() {
        $$("#menu_categories_left a").findBy(exactText("Для стирки")).click();
    }

    public void openPowders() {
        $$("#parameters-filter-form i").findBy(exactText("Порошок")).click();
    }
}
