package ua.com.rozetka.widgets;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Electronics extends Common {

    public void open() {
        $(".logo-link").click();
        $$("nav[class~='m-main'] a").findBy(exactText("Смартфоны, ТВ и электроника")).click();
    }

    public void openPhones() {
        $$("#menu_categories_left a").findBy(exactText("Телефоны")).click();
    }

    public void openSmartphones() {
        $$("#menu_categories_left a").findBy(exactText("Смартфоны")).click();
    }
}
